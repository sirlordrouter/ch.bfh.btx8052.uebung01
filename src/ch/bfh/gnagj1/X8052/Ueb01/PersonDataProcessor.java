package ch.bfh.gnagj1.X8052.Ueb01;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ch.bfh.gnagj1.X8052.Ueb01.exceptions.InvalidLineException;

/**
 * Berner Fachhochschule</br> Medizininformatik BSc</br> Modul 8051-HS2012</br>
 * 
 * <p>
 * A utility-class for reading and writing Persondata from an to a File. The
 * Lineformat must match the following Pattern:</br> [ID as
 * Integer];[Name];[Forename];[a Birthyear];[Gender].</br> The class provides
 * also different Methods for handling and sorting the Data stored in a
 * <code>TreeMap</code>.
 * </p>
 * 
 * @author Johannes Gnaegi, gnaegj1@bfh.ch
 * @version 04-03-2013
 */
public class PersonDataProcessor {
	/** The value indicates the character separating each item in a line. */
	private static final String LINE_SEPARATOR = ";";
	/**
	 * The value indicates the character separating the search-option from the
	 * argument.
	 */
	private static final String OPT_SEPARATOR = ":";
	/** The value indicates the character indicating the search-option for name. */
	private static final String OPT_NAME = "n";
	/**
	 * The value indicates the character indicating the search-option for
	 * forename.
	 */
	private static final String OPT_FORENAME = "f";
	/**
	 * The value indicates the character indicating the search-option for
	 * birthyear.
	 */
	private static final String OPT_YEAROFBIRTH = "y";
	/**
	 * The value indicates the character indicating the search-option for
	 * gender.
	 */
	private static final String OPT_GENDER = "g";
	/**
	 * The value indicates the <a href="../util/regex/Pattern.html#sum">regular
	 * expression</a> which a line must match.
	 */
	private static final Pattern REGEX_LINEFORMAT = Pattern.compile("[0-9]+"
			+ LINE_SEPARATOR + ".+" + LINE_SEPARATOR + ".+" + LINE_SEPARATOR
			+ "[0-9]{4};[MmWw]");
	/**
	 * The value indicates the <a href="../util/regex/Pattern.html#sum">regular
	 * expression</a> which the search-query must match.
	 */
	private static final Pattern REGEX_SEARCHQUERY = Pattern
			.compile("[n|f|y|g]" + OPT_SEPARATOR + ".+");

	/**
	 * Creates a file from a given file-path as <code>String</code> and reads
	 * the file per line. The input file is encoded from <i>ISO-8859-1</i>. Each
	 * line must match the given <a
	 * href="../util/regex/Pattern.html#sum">regular expression</a> in
	 * {@link PersonDataProcessor#REGEX_LINEFORMAT}. If the current line does
	 * not match the given pattern a <code>InvalidLineException</code> is
	 * thrown. In case of an exception the <code>Scanner</code> is closed. Each
	 * read Person is stored in the <code>TreeMap</code>. <i>key</i> is the
	 * Number, the first element in the line. <i>value</i> is the following part
	 * of the line without the <i>;</i> on the beginning.
	 * 
	 * @param filename
	 *            File to read the person data from.
	 * @return <code>TreeMap</code> with all the Persons read from the file. If
	 *         no valid line the <code>TreeMap</code> is empty.
	 * @throws FileNotFoundException
	 *             If not being able to create a File with the given filepath.
	 * @throws InvalidLineException
	 *             If a line does not match the given pattern in
	 *             {@link PersonDataProcessor#REGEX_LINEFORMAT} or the key
	 *             exceeds the value of 2<sup>32</sup>-1.
	 */
	public static TreeMap<Integer, String> buildMapFromFile(String filename)
			throws FileNotFoundException, InvalidLineException {
		int countIgnored = 0;
		TreeMap<Integer, String> personMap = new TreeMap<>();
		Scanner in = null;

		try {
			in = new Scanner(new File(filename), "ISO-8859-1");
			while (in.hasNext()) {
				String s = in.nextLine();
				if (!REGEX_LINEFORMAT.matcher(s).matches()) {
					throw new InvalidLineException();
				}

				int separatorPos = s.indexOf(LINE_SEPARATOR);
				int key;
				try {
					key = Integer.parseInt(s.substring(0, separatorPos));
				} catch (NumberFormatException e) {
					throw new InvalidLineException();
				}

				personMap.put(key, s.substring(separatorPos + 1));
			}
		} finally {
			if (in != null) {
				in.close();
			}
		}

		return personMap;
	}

	/**
	 * Merges the content of two <code>TreeMaps</code> specified as parameter. A
	 * new <code>TreeMap</code> is created first, then the content of
	 * <code>aFirstMap</code> is put into, afterwards the content of the
	 * <code>secondMap</code>. If a <i>key</i> exists already from the first
	 * <code>TreeMap</code> the <i>value</i> from the first Map is overwritten
	 * by the second <code>TreeMap</code> <i>value</i>. The contents of the two
	 * <code>TreeMaps</code> specified does not change.
	 * 
	 * @param aFirstMap
	 *            a <code>TreeMap</code> with or without content.
	 * @param aSecondMap
	 *            a <code>TreeMap</code> with or without content.
	 * @return a <code>TreeMap</code> with the content from the both
	 *         <code>TreeMaps</code> specified in the parameter.
	 */
	public static TreeMap<Integer, String> mergeTwoMaps(
			TreeMap<Integer, String> aFirstMap,
			TreeMap<Integer, String> aSecondMap) {
		TreeMap<Integer, String> mergedMap = new TreeMap<>();
		mergedMap.putAll(aFirstMap);
		mergedMap.putAll(aSecondMap);
		return mergedMap;
	}

	/**
	 * Reverses the order of the values in a specified <code>TreeMap</code>. 
	 * 
	 * @param aMap
	 *  a <code>TreeMap</code> whose order will be inverted. 
	 * @return
	 *  a <code>TreeMap</code> with reversed ordering compared with the Map 
	 *  given as parameter. 
	 */
	public static TreeMap<Integer, String> reverseTreeMap(
			TreeMap<Integer, String> aMap) {
		TreeMap<Integer, String> reversedMap = new TreeMap<Integer, String>(
				Collections.reverseOrder());
		reversedMap.putAll(aMap);
		return reversedMap;
	}

	// Fileformat: 22127;Haefeli;Arnold;1925;M
	/**
	 * Searches a given <code>TreeMap</code> for values matching the specified 
	 * <i>searchOption</i>. 
	 * 
	 * 
	 * <p>A possible usage of the <i>searchOption</i> would be: 
	 * <blockquote><pre>
	 * [Option][Arguments], Example: n:M�ller
	 * </pre></blockquote>
	 * </p>
	 * <h4> Summary of possible Search-Options </h4>
	 * <table border="0" cellpadding="1" cellspacing="0" width="300"
	 *  summary="Regular expression constructs, and what they match">
	 * <tr align="left">
	 * <th bgcolor="#CCCCFF" align="left" id="construct">Option</th>
	 * <th bgcolor="#CCCCFF" align="left" id="matches">Searches in</th>
	 * </tr
	 * <tr><td valign="top" headers="construct characters"><i>n:</i></td>
	 *     <td headers="matches">the part <i>Name</i></td></tr>
	 * <tr><td valign="top" headers="construct characters"><i>f:</i></td>
	 *     <td headers="matches">the part <i>Forename</i></td></tr>
	 * <tr><td valign="top" headers="construct characters"><i>y:</i></td>
	 *     <td headers="matches">the part <i>Birthyear</i></td></tr>
	 * <tr><td valign="top" headers="construct characters"><i>g:</i></td>
	 *     <td headers="matches">the part <i>Gender</i></td></tr>
	 * <tr><th>&nbsp;</th></tr>
	 * </table>
	 * 
	 * @param searchOption
	 *  
	 * @param aMap
	 *  a <code>TreeMap</code> to search in. 
	 * @return
	 *  a new <code>TreeMap</code> containing all the entries matching the 
	 *  <i>searchOption</i>. 
	 *  @throws IllegalArgumentException
	 *   if the searchOption is not in the correct format. 
	 */
	public static TreeMap<Integer, String> searchPersons(String searchOption,
			TreeMap<Integer, String> aMap) throws IllegalArgumentException {
		TreeMap<Integer, String> foundPersons = new TreeMap<>();

		if (!REGEX_SEARCHQUERY.matcher(searchOption).matches()) {
			throw new IllegalArgumentException("Search Query '" + searchOption
					+ "' does not match correct format!");
		}

		String option = searchOption.substring(0, 1);
		String args = searchOption.substring(2);

		int optionID = (OPT_NAME + OPT_FORENAME + OPT_YEAROFBIRTH + OPT_GENDER)
				.indexOf(option);

		// ArrayList oder HashMap testen
		for (Entry<Integer, String> entry : aMap.entrySet()) {
			String value = entry.getValue();
			String item = value.split(LINE_SEPARATOR)[optionID];
			if (item.equals(args)) {
				foundPersons.put(entry.getKey(), value);
			}
		}
		return foundPersons;
	}

	/**
	 * 
	 * @param aFilename
	 * @param aMap
	 * @throws FileNotFoundException
	 * @throws UnsupportedEncodingException
	 */
	public static void writeMapToFile(String aFilename,
			TreeMap<Integer, String> aMap) throws FileNotFoundException,
			UnsupportedEncodingException {

		PrintWriter out = null;

		try {
			out = new PrintWriter(aFilename, "ISO-8859-1");
			for (Entry<Integer, String> entry : aMap.entrySet()) {
				String s = entry.getKey() + LINE_SEPARATOR + entry.getValue();
				out.println(s);
			}

		} finally {
			if (out != null) {
				out.close();
			}
		}
	}

	/**
	 * 
	 * @param aFilename
	 * @param aTreeSet
	 * @throws FileNotFoundException
	 * @throws UnsupportedEncodingException
	 */
	public static void writeNotUsedKeysToFile(String aFilename,
			TreeSet<Integer> aTreeSet) throws FileNotFoundException,
			UnsupportedEncodingException {
		PrintWriter out = null;

		try {
			out = new PrintWriter(aFilename, "ISO-8859-1");
			for (Integer entry : aTreeSet) {
				out.println(entry);
			}

		} finally {
			if (out != null) {
				out.close();
			}
		}
	}

	/**
	 * 
	 * @param aMap
	 * @param startIndex
	 * @param endIndex
	 * @return
	 */
	public static TreeSet<Integer> writeNotUsedKeysToSet(
			TreeMap<Integer, String> aMap, int startIndex, int endIndex) {
		TreeSet<Integer> notUsedKeys = new TreeSet<>();
		if (endIndex <= startIndex) {
			throw new NumberFormatException();
		}
		for (int key = startIndex; key <= endIndex; key++) {
			if (!aMap.containsKey(key)) {
				notUsedKeys.add(key);
			}
		}
		return notUsedKeys;
	}
}

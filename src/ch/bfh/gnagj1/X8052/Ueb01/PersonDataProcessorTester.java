package ch.bfh.gnagj1.X8052.Ueb01;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.*;

import ch.bfh.gnagj1.X8052.Ueb01.exceptions.InvalidLineException;

/**
 * Berner Fachhochschule</br> Medizininformatik BSc</br> Modul 8051-HS2012</br>
 * 
 * <p>
 * Class Description
 * </p>
 * 
 * @author Johannes Gnaegi, gnaegj1@bfh.ch
 * @version 20-02-2013
 */

public class PersonDataProcessorTester {

	/**
	 * @param args
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 * @throws InvalidLineException 
	 */
	public static void main(String[] args) throws FileNotFoundException,
			UnsupportedEncodingException, InvalidLineException {

		long starttime = System.currentTimeMillis();
		long init = starttime;
		TreeMap<Integer, String> aMap = PersonDataProcessor.buildMapFromFile("/Users/Johannes/Downloads/DataMnCSV.txt");
		System.out.println("mapMen: Map was built");
		System.out.println("Execution time: "
				+ (System.currentTimeMillis() - starttime) + " ms");
		System.out.println("mapMen: first used key ‐> " + aMap.firstKey());
		System.out.println("mapMen: last used key ‐> " + aMap.lastKey());
		System.out.println("mapMen: mapsize ‐> " + aMap.size());
		starttime = System.currentTimeMillis();
			PersonDataProcessor.writeMapToFile(
					"/Users/Johannes/Downloads/DataMnCSVout.txt", aMap);
			System.out.println("mapMen: Map was written to File");
			System.out.println("Execution time: "
					+ (System.currentTimeMillis() - starttime) + " ms");

		starttime = System.currentTimeMillis();
		TreeSet<Integer> notUsedKeys = PersonDataProcessor.writeNotUsedKeysToSet(aMap,
				17530, 17550);
		System.out
				.println("mapMen: number of unused keys between 17530 ‐ 17550 ‐> "
						+ notUsedKeys.size());
		System.out
				.println("mapMen: first unused key ‐> " + notUsedKeys.first());
		System.out.println("mapMen: last unused key ‐> " + notUsedKeys.last());
		PersonDataProcessor.writeNotUsedKeysToFile(
				"/Users/Johannes/Downloads/DataMnCSVnotusedKeys.txt",
				notUsedKeys);
		System.out.println("mapMen: NotUsedKeys was written to File");
		System.out.println("Execution time: "
				+ (System.currentTimeMillis() - starttime) + " ms" + "\n\n\n");

		starttime = System.currentTimeMillis();
		TreeMap<Integer, String> aSecondMap = PersonDataProcessor
				.buildMapFromFile("/Users/Johannes/Downloads/DataWnCSV.txt");
		System.out.println("mapWmn: Map was built");
		System.out.println("Execution time: "
				+ (System.currentTimeMillis() - starttime) + " ms");

		starttime = System.currentTimeMillis();
		System.out
				.println("mapWmn: first used key ‐> " + aSecondMap.firstKey());
		System.out.println("mapWmn: last used key ‐> " + aSecondMap.lastKey());
		System.out.println("mapWmn: mapsize ‐> " + aSecondMap.size());
			PersonDataProcessor.writeMapToFile(
					"/Users/Johannes/Downloads/DataWnCSVout.txt", aSecondMap);
			System.out.println("mapWmn: Map was written to File");
			System.out.println("Execution time: "
					+ (System.currentTimeMillis() - starttime) + " ms");

		starttime = System.currentTimeMillis();
		notUsedKeys = PersonDataProcessor.writeNotUsedKeysToSet(aSecondMap, 90, 21150);
		System.out
				.println("mapWmn: number of unused keys between 90 ‐ 21150 ‐> "
						+ notUsedKeys.size());
		System.out
				.println("mapWmn: first unused key ‐> " + notUsedKeys.first());
		System.out.println("mapWmn: last unused key ‐> " + notUsedKeys.last());

		PersonDataProcessor.writeNotUsedKeysToFile(
				"/Users/Johannes/Downloads/DataWnCSVnotusedKeys.txt",
				notUsedKeys);
		System.out.println("mapWmn: NotUsedKeys was written to File");
		System.out.println("Execution time: "
				+ (System.currentTimeMillis() - starttime) + " ms" + "\n\n\n");

		starttime = System.currentTimeMillis();
		TreeMap<Integer, String> mergedMap = PersonDataProcessor.mergeTwoMaps(aMap,
				aSecondMap);
		System.out.println("mapMerged: Map was built");
		System.out.println("Execution time: "
				+ (System.currentTimeMillis() - starttime) + " ms");

		starttime = System.currentTimeMillis();
		System.out.println("mergedMap: first used key ‐> "
				+ mergedMap.firstKey());
		System.out
				.println("mergedMap: last used key ‐> " + mergedMap.lastKey());
		System.out.println("mergedMap: mapsize ‐> " + mergedMap.size());

			PersonDataProcessor
					.writeMapToFile(
							"/Users/Johannes/Downloads/DataMergedCSVout.txt",
							mergedMap);
			System.out.println("mergedMap: Map was written to File");
			System.out.println("Execution time: "
					+ (System.currentTimeMillis() - starttime) + " ms");

		starttime = System.currentTimeMillis();
		notUsedKeys = PersonDataProcessor.writeNotUsedKeysToSet(mergedMap, 1, 45000);
		System.out
				.println("mergedMap: number of unused keys between 1 ‐ 45000 ‐> "
						+ notUsedKeys.size());
		System.out.println("mergedMap: first unused key ‐> "
				+ notUsedKeys.first());
		System.out.println("mergedMap: last unused key ‐> "
				+ notUsedKeys.last());

		PersonDataProcessor.writeNotUsedKeysToFile(
				"/Users/Johannes/Downloads/DataMergedCSVnotusedKeys.txt",
				notUsedKeys);
		System.out.println("mergedMap: NotUsedKeys was written to File");
		System.out.println("Execution time: "
				+ (System.currentTimeMillis() - starttime) + " ms");

		starttime = System.currentTimeMillis();
		System.out.println("mergedMap: person with the ID 8603: ‐> "
				+ mergedMap.get(8603));
		System.out.println("mergedMap: person with the ID 45: ‐> "
				+ mergedMap.get(45));
		TreeMap<Integer, String> mapPersons = PersonDataProcessor.searchPersons(
				"n:Müller", mergedMap);
		System.out
				.println("mapMerged: How many lines Match the pattern n:Müller: ->"
						+ mapPersons.size());
		PersonDataProcessor.writeMapToFile(
				"/Users/Johannes/Downloads/DataMergedCSVMergedPatter1.txt",
				mapPersons);
		System.out.println("mapPersons(n:Müller): was written to File");
		mapPersons = PersonDataProcessor.searchPersons("f:Beat", mapPersons);
		System.out
				.println("mapMerged: How many lines Match the pattern n:Müller AND f:Beat ->"
						+ mapPersons.size());
		PersonDataProcessor.writeMapToFile(
				"/Users/Johannes/Downloads/DataMergedCSVMergedPatter2.txt",
				mapPersons);
		System.out.println("mapPersons(n:Müller f:Beat): was written to File");
		System.out.println("Execution time: "
				+ (System.currentTimeMillis() - starttime) + " ms");

		starttime = System.currentTimeMillis();
		mapPersons = PersonDataProcessor.searchPersons("y:1964", mergedMap);
		System.out
				.println("mapMerged: How many lines Match the pattern y:1964: ->"
						+ mapPersons.size());
		PersonDataProcessor.writeMapToFile(
				"/Users/Johannes/Downloads/DataMergedCSVMergedPatter3.txt",
				mapPersons);
		System.out.println("mapPersons(y:1964): was written to File");
		System.out.println("Execution time: "
				+ (System.currentTimeMillis() - starttime) + " ms" + "\n\n\n");

		starttime = System.currentTimeMillis();
		TreeMap<Integer, String> mergedMapRev = PersonDataProcessor
				.reverseTreeMap(mergedMap);
		System.out.println("mergedMapRev: first used key ‐> "
				+ mergedMapRev.firstKey());
		System.out.println("mergedMapRev: last used key ‐> "
				+ mergedMapRev.lastKey());
		System.out.println("mergedMapRev: mapsize ‐> " + mergedMapRev.size());


			PersonDataProcessor.writeMapToFile(
					"/Users/Johannes/Downloads/DataMergedRevCSVout.txt",
					mergedMapRev);
			System.out.println("mergedMapRev: Map was written to File");
			System.out.println("Execution time: "
					+ (System.currentTimeMillis() - starttime) + " ms" + "\n\n\n");


		starttime = System.currentTimeMillis();
		System.out.println("Are the base maps unchanged?");
		System.out.println("mapMen: first used key ‐> " + aMap.firstKey());
		System.out.println("mapMen: last used key ‐> " + aMap.lastKey());
		System.out.println("mapMen: mapsize ‐> " + aMap.size());
		System.out
				.println("mapWmn: first used key ‐> " + aSecondMap.firstKey());
		System.out.println("mapWmn: last used key ‐> " + aSecondMap.lastKey());
		System.out.println("mapWmn: mapsize ‐> " + aSecondMap.size());
		System.out.println("mergedMap: first used key ‐> "
				+ mergedMap.firstKey());
		System.out
				.println("mergedMap: last used key ‐> " + mergedMap.lastKey());
		System.out.println("mergedMap: mapsize ‐> " + mergedMap.size());
		System.out.println("Execution time: "
				+ (System.currentTimeMillis() - starttime) + " ms" + "\n\n\n");

		System.out.println("Total Execution time: "
				+ (System.currentTimeMillis() - init) + " ms" + "\n\n\n");

		
		starttime = System.currentTimeMillis();
		System.out
		.println("mapMerged: search n:Müller");
		TreeMap<Integer, String> foundPersons2 = PersonDataProcessor.searchPersons("n:Müller", mergedMap);
		System.out
		.println("mapMerged: search n:Müller found->" + foundPersons2.size());
		System.out.println("Execution time: "
				+ (System.currentTimeMillis() - starttime) + " ms" + "\n\n\n");
		
		
		//Getting the runtime reference from system
        Runtime runtime = Runtime.getRuntime();
        System.out.println("##### Heap utilization statistics [MB] #####");
        int mb = 1024*1024;
        //Print used memory
        System.out.println("Used Memory:"
            + (runtime.totalMemory() - runtime.freeMemory()) / mb + " MB");
        //Print free memory
        System.out.println("Free Memory:"
            + runtime.freeMemory() / mb+ " MB");      
        //Print total available memory
        System.out.println("Total Memory:" + runtime.totalMemory() / mb+ " MB");
        //Print Maximum available memory
        System.out.println("Max Memory available:" + runtime.maxMemory() / mb+ " MB");
	}

}

package ch.bfh.gnagj1.X8052.Ueb01.exceptions;

public class InvalidLineException extends Exception {

	private static final long serialVersionUID = 1L;

	public InvalidLineException() {
		super();
	}

	public InvalidLineException(Throwable cause) {
		super(cause);
	}
}

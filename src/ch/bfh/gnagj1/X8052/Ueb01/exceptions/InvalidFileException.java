package ch.bfh.gnagj1.X8052.Ueb01.exceptions;

public class InvalidFileException extends Exception {

	private static final long serialVersionUID = 1L;

	public InvalidFileException() {
		super();
	}

	public InvalidFileException(String message) {
		super(message);
	}
}
